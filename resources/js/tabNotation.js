
var EnglishToSpanish = function(){}

EnglishToSpanish.A="LA";
EnglishToSpanish.B="SI";
EnglishToSpanish.C="DO";
EnglishToSpanish.D="RE";
EnglishToSpanish.E="MI";
EnglishToSpanish.F="FA";
EnglishToSpanish.G="SOL";

var SpanishToEnglish = function(){}

SpanishToEnglish.LA="A";
SpanishToEnglish.SI="B";
SpanishToEnglish.DO="C";
SpanishToEnglish.RE="D";
SpanishToEnglish.MI="E";
SpanishToEnglish.FA="F";
SpanishToEnglish.SO="G";


function changeNotation(){
	var actualNotation = $('#notation');
	var chords = $('.chord');
	
	
	if(actualNotation.val() == "spanish")
	{
		chords.each(function (){
			var chordsToChange = $(this).html().split('/');
			var finalChord = "";
			for(i=0;i<chordsToChange.length;i++)
			{
				if(i>0)
					finalChord+='/';
				var simpleChordName = chordsToChange[i];
				var chordLetter = simpleChordName.substring(0,2);
				var newChordName = SpanishToEnglish[chordLetter];
				if(newChordName!=undefined)
				{
					if(newChordName=="G")
						finalChord+=newChordName+simpleChordName.substring(3);
					else
						finalChord+=newChordName+simpleChordName.substring(2);
				}
				else{
					alert ('Se ha detectado un error al parsear el acorde '+chordLetter+'.Puedes editar el tab para corregirlo.');
				}
			}
			
			$(this).html(finalChord);
		});
		actualNotation.val('english');
	}
	else
	{
		chords.each(function (){
			var chordsToChange = $(this).html().split('/');
			var finalChord = "";
			for(i=0;i<chordsToChange.length;i++)
			{
				if(i>0)
					finalChord+='/';
				var simpleChordName = new String(chordsToChange[i]);
				var chordLetter = simpleChordName.charAt(0);
				var newChordName = EnglishToSpanish[chordLetter];
				if(newChordName!=undefined)
				{
					finalChord+=newChordName+simpleChordName.substring(1);
				}	
				else{
					alert ('Se ha detectado un error al parsear el acorde '+chordLetter+'. Puedes editar el tab para corregirlo.');
				}	
			}	
			$(this).html(finalChord);
		});
		actualNotation.val('spanish');
	}
}
