<?php

/**
 * Clase de negocio que representa la tabla chord
 * de la base de datos
 * @package model
 */
class ChordDefinitionBO extends CI_Model {

    var $idChord;
    var $definition;

    function __construct() {
        // Call the Model constructor
        parent::__construct();
    }

    /**
     * Busca todos los acordes registrados en la base de datos.
     * Busca los datos chordName y chordDefinition.
     * @return mixed Array de la forma : nombreDelAcorde -> Definicion
     */
    public function getChords() {
        $sqlQuery = 'select CHORD.NAME as CHORD_NAME, 
					CHORD.DEFINITION as CHORD_DEFINITION
					from CHORD';
        $result = $this->db->query($sqlQuery);
        $chordDefinitions = array();
        foreach ($result->result_array() as $chord) {
            $chordDefinitions[$chord['CHORD_NAME']] = $chord['CHORD_DEFINITION'];
        }
        return $chordDefinitions;
    }

}

?>