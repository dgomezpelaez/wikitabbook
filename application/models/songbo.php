<?php

class SongBO extends CI_Model {

    const VIEW = '(select TR.TAB_ID AS TAB_ID,
					TR.ID AS TAB_REV_ID,
					TAB_REV_ARTIST.ARTIST_ID AS ARTIST_ID
					from (TAB T join (TAB_REV TR join TAB_REV_ARTIST 
						on((TR.ID = TAB_REV_ARTIST.TAB_REV_ID))))
					where ((TR.TAB_ID = T.ID) 
					and (TR.ID = (select max(TAB_REV.ID)
					AS TAB_REV_ID from TAB_REV 
					where (TR.TAB_ID = TAB_REV.TAB_ID))))) TAB_PERFORMER ';

    var $id;
    var $title;

    const TABLE = 'SONG';
    const TITLE = 'TITLE';
    const ID = 'ID';

    function __construct() {
// Call the Model constructor
        parent::__construct();
    }

    function find($title) {
        $sqlQuery = 'select ARTIST.NAME as ARTIST_NAME,
					ARTIST.ID as ID_ARTIST, 
					SONG.TITLE as SONG_TITLE, 
					TAB.ID as TAB_ID 
					from  (((SONG inner join TAB on SONG.ID = TAB.SONG_ID)
	 						 inner join ' . SongBO::VIEW . ' on TAB.ID=TAB_PERFORMER.TAB_ID)
	   						 inner join ARTIST on TAB_PERFORMER.ARTIST_ID=ARTIST.ID)
							 where SONG.TITLE like "%' . $this->db->escape_like_str($title) . '%" order by ARTIST.NAME';
        log_message('debug', $sqlQuery);
        $query = $this->db->query($sqlQuery);
        return $query->result_array();
    }

    function lastFive() {
        $sqlQuery = 'select ARTIST.NAME as ARTIST_NAME,
					ARTIST.ID as ARTIST_ID, 
					SONG.TITLE as SONG_TITLE, 
					TAB.ID as TAB_ID 
					from  (((SONG inner join TAB on SONG.ID = TAB.SONG_ID)
	 						 inner join ' . SongBO::VIEW . ' on TAB.ID=TAB_PERFORMER.TAB_ID)
	   						 inner join ARTIST on TAB_PERFORMER.ARTIST_ID=ARTIST.ID)
							 order by TAB.ID DESC limit 5';
        log_message('debug', $sqlQuery);
        $query = $this->db->query($sqlQuery);
        return $query->result_array();
    }

    function insert() {

        $this->db->insert(self::TABLE, $this);

        return $this->db->insert_id();
    }

      function findTabBySongTitle($title){
          $sqlQuery = 'select TAB.ID as ID 
					from  SONG inner join TAB on SONG.ID = TAB.SONG_ID 
                                        where SONG.TITLE ="' . $this->db->escape_like_str($title) . '" limit 1';
          $query = $this->db->query($sqlQuery);
        if ($query->num_rows == 0) {
            log_message('debug', 'Tab Show Compatibility Error: Find song by title fails.');
        } else {
            log_message('debug', 'Tab Show Compatibility: OK! We find the song by title');
        }
        return $query->row_array();
    }
}

?>
