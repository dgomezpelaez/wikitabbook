<?php

class ArtistAlbumBO extends CI_Model {

    var $albumId;
    var $artistId;

    const TABLE = 'ARTIST_ALBUM';
    const ALBUM_ID = 'ALBUM_ID';
    const ARTIST_ID = 'ARTIST_ID';

    function __construct() {
        // Call the Model constructor
        parent::__construct();
    }

    function getRelationship($artistId, $albumId) {
        $sql = 'select * 			
		from ' . ArtistAlbumBO::TABLE . ' where ' . ArtistAlbumBO::ALBUM_ID . '=? and ' . ArtistAlbumBO::ARTIST_ID . '=?';

        $result = $this->db->query($sql, array(intval($albumId), intval($artistId)));

        return $result->result_array();
    }

    function saveRelationship($artistId, $albumId) {
        if (sizeof($this->getRelationship($artistId, $albumId)) == 0) {
            $data = array('ALBUM_ID' => $albumId,
                'ARTIST_ID' => $artistId);
            $this->db->insert(ArtistAlbumBO::TABLE, $data);
        }
    }

}

?>
