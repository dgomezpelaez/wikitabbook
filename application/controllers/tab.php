<?php
require_once APPPATH.'/third_party/twitteroauth/twitteroauth/twitteroauth.php';
require_once APPPATH.'/third_party/twitteroauth/twitteroauth/OAuth.php';

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Tab extends CI_Controller {

    //put your code here
    public function __construct() {
        parent::__construct();
        $this->load->model('songBO');
        $this->load->model('albumBO');
        $this->load->model('artistBO');
        $this->load->model('artistalbumBO');
        $this->load->model('tabBO');
        $this->load->model('tabRevBO');
        $this->load->model('tabRevArtistBO');
        $this->load->model('tabRevAlbumBO');
        $this->load->model('chordDefinitionBO');
        $this->load->helper(array('form', 'html'));

        $this->load->model('remembermeBO');

        if (isset($_COOKIE['wikitoken'])) {

            $rememberme = $this->remembermeBO->loadByToken($_COOKIE['wikitoken']);
            if ($rememberme != null && sizeof($rememberme) > 0) {
                $this->session->set_userdata('username', $_COOKIE['wikiusername']);
                $this->session->set_userdata('userId', $_COOKIE['wikiuserid']);
            }
        }
    }

    private function publicTweet($message){

        $connection = $this->getConnectionWithAccessToken("988772058-mZkvRIyBNO2c2IIBIHRTpe8CONTKC8QX50MgSETc", "e0p6uSIpU3LRAjDomdaSHKKbm8Z1l9np60QSOnAtRyw");

        $content = $connection->post("statuses/update",array('status' => $message));
        echo $content;
    }
    
    private function checkAuthorization($pageFrom) {
        $user = $this->session->userdata('userId');
        if (!$user) {
            $this->session->set_userdata('login_error', 'Para crear o editar tabs tienes que estar logueado.');
            $this->session->set_userdata('pageAfterLogin', $pageFrom);
            $this->load->helper('url');
            header('Location:' . site_url("register/loginForm/"));
        }
        return;
    }

    public function form() {
        $this->checkAuthorization('tab/form');
        $this->load->view('tab/form');
    }

    public function editForm($tabRevId) {

        $data['onlyBody'] = TRUE;


        $this->tabRevBO->id = $tabRevId;
        $tabRev = $this->tabRevBO->load();
        $data['tab'] = $tabRev[TabRevBO::TAB];
        $data['songTitle'] = $tabRev[SongBO::TITLE];
        $data['tabId'] = $tabRev['TAB_ID'];
        $data['notation'] = $tabRev['NOTATION'];
        $data['commitComment'] = $tabRev['COMMIT_COMMENT'];
        $artists = $this->tabRevBO->getArtists();
        $albums = $this->tabRevBO->getAlbums();
        $data['artists'] = $artists;
        $data['albums'] = $albums;
        $this->load->view('tab/editForm', $data);
    }

    public function create() {

        $this->load->library('form_validation');
        //   $this->form_validation->set_rules('artists', 'Artists', 'callback_artistsCheck');

        $this->form_validation->set_error_delimiters('<span class="help-inline error">', '</span>');
        if ($this->form_validation->run() == FALSE) {

            $this->load->view('tab/form');
            return;
        }

        $this->db->trans_start();

        $this->songBO->title = $this->input->post('songTitle');
        $this->tabBO->song_id = $this->songBO->insert();
        $this->tabRevBO->tab_id = $this->tabBO->insert();

        $this->saveRevision();
        $tabLink = site_url("tab/show/" . urlencode($this->input->post('songTitle')) . "/" . $this->tabRevBO->tab_id);
        
        $this->publicTweet('Nuevo tab en WikiTabBook! - '.$this->input->post('songTitle').' -> '.$tabLink);

        $this->db->trans_complete();

        $this->load->helper('url');
        header('Location:' . site_url("tab/show/" . urlencode($this->input->post('songTitle')) . "/" . $this->tabRevBO->tab_id));
    }
    
    
     public function update() {

        $this->db->trans_start();


        $this->tabRevBO->tab_id = $this->input->post('tabId');

        $this->saveRevision();
        $tabLink = site_url("tab/show/" . urlencode($this->input->post('songTitle')) . "/" . $this->tabRevBO->tab_id);
        
        $this->db->trans_complete();

        $this->load->helper('url');
        header('Location:' . site_url("tab/show/" . urlencode($this->input->post('songTitle')) . "/" . $this->tabRevBO->tab_id));
    }
    
    function getConnectionWithAccessToken($oauth_token, $oauth_token_secret) {

           $connection = new TwitterOAuth('FgX0v6VMvA84yeCT4Vzg', '8T3qQ1wsbP7DArJeJhPKjQVEvXMbj67u0tuK5kP3XQ', $oauth_token, $oauth_token_secret);

            return $connection;
     }

    private function saveRevision() {

        $artists = $this->input->post('artistId');
        $albums = $this->input->post('albumId');
        $newArtists = $this->input->post('artist_new_names');
        $newAlbums = $this->input->post('album_new_names');
        $albumsOrArtists = array(&$artists, &$albums, &$newAlbums, &$newArtists);
        $this->convertArtistsAndAlbumsToArrays($albumsOrArtists);
        if (sizeof($newAlbums) > 0) {
            $this->addAlbums($newAlbums, $albums);
        }
        if (sizeof($newArtists) > 0) {
            $this->addArtists($newArtists, $artists);
        }
        $this->makeRelationships($artists, $albums);

        $commitComment = $this->input->post('commitComment');
        $this->tabRevBO->user_id = intval($this->session->userdata('userId'));
        if(!$commitComment){
           $commitComment = 'Tab inicial';
        }
        $this->tabRevBO->commit_comment = $commitComment;
        $this->tabRevBO->date = date('d/m/y G:i');
        $this->tabRevBO->notation = $this->input->post('tabNotation');
        $this->tabRevBO->tab = $this->input->post('tab');
        $idTabRev = $this->tabRevBO->insert();
        $this->tabRevArtistBO->tab_rev_id = $idTabRev;
        $this->tabRevAlbumBO->tab_rev_id = $idTabRev;

        foreach ($artists as $artist) {
            $this->tabRevArtistBO->artist_id = intval($artist);
            $this->tabRevArtistBO->insert();
        }

        foreach ($albums as $album) {
            $this->tabRevAlbumBO->album_id = intval($album);
            $this->tabRevAlbumBO->insert();
        }
    }

   

    public function artistsCheck() {
        $artists = $this->input->post('artistId');

        $newArtists = $this->input->post('artist_new_names');
        if ($artists == false && $newArtists == false) {
            $this->form_validation->set_message('artistsCheck', 'Selecciona o crea el artista');
            return FALSE;
        } else {
            return TRUE;
        }
    }

    public function prev() {
        $artists = $this->input->post('artistId');
        $albums = $this->input->post('albumId');

        $this->load->library('renderChordproHtml');
        if ($this->session->userdata('CHORD_DEFINITIONS') == null) {
            $this->session->set_userdata('CHORD_DEFINITIONS', $this->chordDefinitionBO->getChords());
        }

        $tab = $this->renderchordprohtml->parseChordpro($this->input->post('tab'));
        if (!$artists || !$albums) {
            $tab = '<span class="alert-error">Ey, no has seleccionado artista o disco, ¿seguro que no lo sabes?</span>' . $tab;
        }
        echo $tab;
    }

    function show($title, $id) {
        $this->load->model('userBO');
        $this->load->library('renderChordproHtml');
        if ($this->session->userdata('CHORD_DEFINITIONS') == null) {
            $this->session->set_userdata('CHORD_DEFINITIONS', $this->chordDefinitionBO->getChords());
        }

        $this->tabRevBO->tab_id = $id;
        $tabRev = $this->tabRevBO->loadLastRevision();
        $this->tabRevBO->id = $tabRev['ID'];
        $data['tab'] = $this->renderchordprohtml->parseChordpro($tabRev[TabRevBO::TAB]);
        $data['songTitle'] = $tabRev[SongBO::TITLE];
        $data['tabId'] = $tabRev['TAB_ID'];
        $data['tabRevId'] = $tabRev['ID'];
        $data['notation'] = $tabRev['NOTATION'];


        $artists = $this->tabRevBO->getArtists();
        $songTitles = $this->session->userdata('SONG_TITLES');
        $songTitles[$tabRev['TAB_ID']] = $tabRev[SongBO::TITLE];
        $songTitles = $this->session->userdata('SONG_TITLES', $songTitles);
        $artistsNames = '';
        foreach ($artists as $artist) {
            if ($artistsNames != '') {
                $artistsNames = $artistsNames . '/';
            }
            $artistsNames = $artistsNames . $artist['ARTIST_NAME'];
        }
        $data['artistsNames'] = $artistsNames;
        $pageTitle = $tabRev[SongBO::TITLE] . ' - ' . $artistsNames;
        $data['pageTitle'] = $pageTitle;
        $pageTitles = $this->session->userdata('PAGE_TITLES');
        $pageTitles[$tabRev['TAB_ID']] = $pageTitle;
        $this->session->userdata('PAGE_TITLES', $pageTitles);


        if ($this->userBO->isFav($this->session->userdata('userId'), $this->tabRevBO->tab_id)) {
            $data['isFav'] = TRUE;
        }

        $this->load->view('tab/showTabs', $data);
    }
    
       function showWithoutId($title) {
      

        $title = $this->encodeParam($title);
       
	$tab = $this->songBO->findTabBySongTitle($title);
        if($tab==null || sizeof($tab)==0)
        {
            $data['error_msg']='No podemos encontrar este tab en WikiTabBook. ¿Has probado con la búsqueda?';
            $this->load->view('error',$data);
            return;
        }
        $this->show($title, $tab[TabBO::ID]);
    }

    function showRevision($tabRevId) {
        $this->load->model('userBO');
        $this->load->library('renderChordproHtml');
        if ($this->session->userdata('CHORD_DEFINITIONS') == null) {
            $this->session->set_userdata('CHORD_DEFINITIONS', $this->chordDefinitionBO->getChords());
        }

        $this->tabRevBO->id = $tabRevId;
        $tabRev = $this->tabRevBO->load();
        $this->tabRevBO->id = $tabRev['ID'];
        $data['tab'] = $this->renderchordprohtml->parseChordpro($tabRev[TabRevBO::TAB]);
        $data['songTitle'] = $tabRev[SongBO::TITLE];
        $data['tabId'] = $tabRev['TAB_ID'];
        $data['tabRevId'] = $tabRev['ID'];
        $data['notation'] = $tabRev['NOTATION'];


        $artists = $this->tabRevBO->getArtists();
        $songTitles = $this->session->userdata('SONG_TITLES');
        $songTitles[$tabRev['TAB_ID']] = $tabRev[SongBO::TITLE];
        $songTitles = $this->session->userdata('SONG_TITLES', $songTitles);
        $artistsNames = '';
        foreach ($artists as $artist) {
            if ($artistsNames != '') {
                $artistsNames = $artistsNames . '/';
            }
            $artistsNames = $artistsNames . $artist['ARTIST_NAME'];
        }
        $data['artistsNames'] = $artistsNames;
        $pageTitle = $tabRev[SongBO::TITLE] . ' - ' . $artistsNames;
        $data['pageTitle'] = $pageTitle;
        $pageTitles = $this->session->userdata('PAGE_TITLES');
        $pageTitles[$tabRev['TAB_ID']] = $pageTitle;
        $this->session->userdata('PAGE_TITLES', $pageTitles);


        if ($this->userBO->isFav($this->session->userdata('userId'), $this->tabRevBO->tab_id)) {
            $data['isFav'] = TRUE;
        }

        $this->load->view('tab/showTabs', $data);
    }

    function history($tabId) {
        $this->tabRevBO->tab_id = $tabId;
        $history = $this->tabRevBO->history();
        $data['history'] = $history;
        $data['onlyBody'] = TRUE;
        $this->load->view('tab/history', $data);
    }

    function diff($tabRevId1, $tabRevId2) {
        $this->tabRevBO->id = $tabRevId1;
        $tabRev1 = $this->tabRevBO->load();
        $this->tabRevBO->id = $tabRevId2;
        $tabRev2 = $this->tabRevBO->load();
        $this->load->library('diff');
        $data['tabRev1'] = $tabRev1;
        $data['tabRev2'] = $tabRev2;
        $data['diff'] = Diff::toTable(Diff::compare($tabRev2['TAB'], $tabRev1['TAB']));
        $this->load->view('tab/diff', $data);
    }

    function convertArtistsAndAlbumsToArrays(&$albumsAndArtists) {
        foreach ($albumsAndArtists as &$albumOrArtist) {
            if (isset($albumOrArtist)) {
                if (!is_array($albumOrArtist) && $albumOrArtist) {
                    $albumOrArtist = array($albumOrArtist);
                } else if (!$albumOrArtist) {
                    $albumOrArtist = array();
                }
            } else {
                $albumOrArtist = array();
            }
        }
    }

    function makeRelationships(&$artists, &$albums) {
        //Add "indefinido" if there is no artist or album 
        if (sizeof($artists) == 0) {
            $this->addElementToAnArray(1, $artists);
        }
        if (sizeof($albums) == 0) {
            $this->addElementToAnArray(1, $albums);
        }
        foreach ($artists as $aritst) {
            foreach ($albums as $album) {
                $this->artistalbumBO->saveRelationship($aritst, $album);
            }
        }
    }

    function addAlbums($newAlbums, &$albums) {
        foreach ($newAlbums as $newAlbum) {
            $newAlbumFindResult = $this->albumBO->findExactly($newAlbum);
            if ($newAlbumFindResult != null) {
                //This albums exists!
                addElementToAnArray($newAlbumFindResult['id'], $albums);
            } else {
                //Save this album
                $this->albumBO->title = $newAlbum;
                $id = $this->albumBO->insert();
                $this->addElementToAnArray($id, $albums);
            }
        }
    }

    function addArtists($newArtists, &$artists) {
        foreach ($newArtists as $newArtist) {

            $newArtistFindResult = $this->artistBO->findExactly($newArtist);
            if ($newArtistFindResult != null) {
                //This albums exists!
                $this->addElementToAnArray($newArtistFindResult['id'], $artists);
            } else {
                //Save this album
                $this->artistBO->name = $newArtist;
                $id = $this->artistBO->insert();
                $this->addElementToAnArray($id, $artists);
            }
        }
    }

    function addElementToAnArray($element, &$array) {
        if (!is_array($array) && isset($array)) {
            $array = array($array);
        } else if (!isset($array)) {
            $array = array();
        }
        array_push($array, $element);
    }

    
    private function encodeParam($param)
	{
		$encodeParam = urldecode($param);
                $encodeParam = str_replace('&#40;', '(', $encodeParam);
                $encodeParam = str_replace('&#41;', ')', $encodeParam);
		return $encodeParam;
	}
	
}
