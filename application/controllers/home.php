<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Home extends CI_Controller {

    //put your code here
    public function __construct() {
        parent::__construct();

        $this->load->model('remembermeBO');

        if (isset($_COOKIE['wikitoken'])) {

            $rememberme = $this->remembermeBO->loadByToken($_COOKIE['wikitoken']);
            if ($rememberme != null && sizeof($rememberme) > 0) {
                $this->session->set_userdata('username', $_COOKIE['wikiusername']);
                $this->session->set_userdata('userId', $_COOKIE['wikiuserid']);
            }
        }
    }

    public function index() {
        $this->load->model('userBO');
        $this->load->model('songBO');
        $this->load->model('tabBO');
        $users = $this->userBO->findMoreCollaboratives();
        $data['users'] = $users;
        $data['numTabs'] = $this->tabBO->count();
        $data['lastFive'] = $this->songBO->lastFive();
        $loginError = $this->session->userdata('login_error');
        if ($loginError != null) {
            $data['login_error'] = $loginError;
            $this->session->unset_userdata('login_error');
        }
        $this->load->view('home', $data);
    }

    public function lastChanges() {

        $this->load->model('tabRevBO');
        $data = array();
        $data['lastChanges'] = $this->tabRevBO->lastChanges();
        $this->load->view('lastChanges', $data);
    }

    public function help() {

        $this->load->view('help');
    }

    public function search() {
        $this->load->library('pagination');
        $this->load->library('uri');
        $nexts = $this->uri->segment(3, 0);

        $config['base_url'] = 'http://localhost/wiki/home/search/';
        $config['total_rows'] = 60;
        $config['per_page'] = 20;
        $this->pagination->initialize($config);
        $this->load->model('songBO');
        $data = array();
        $searchIn = $this->input->get('searchIn', TRUE);
        $searchValue = $this->input->get('search', TRUE);
        $data['searchValue'] = $searchValue;
        if ($searchIn == null) {
            log_message('debug', 'Vamos a buscar en canciones con el parámetro: ' . $searchValue);
            $songResults = $this->songBO->find($searchValue);
            $data['songResults'] = $songResults;
            $this->load->view('searchResults', $data);
        } else if ($searchIn == 'artists') {
            $this->load->model('artistBO');
            $artistResults = $this->artistBO->find($searchValue);
            $data['artistResults'] = $artistResults;
            $this->load->view('searchResultsDinamic', $data);
        } else if ($searchIn == 'albums') {
            $this->load->model('albumBO');
            $albumResults = $this->albumBO->find($searchValue);
            $data['albumResults'] = $albumResults;
            $this->load->view('searchResultsDinamic', $data);
        }
    }

}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */