<?php
require ('application/views/tiles/head.php');
require('application/views/tiles/header.php');
require('application/views/tiles/menu.php');
?>
<div class="fieldset fieldset-register">

    <form class="form-horizontal" method="post" action="<?= site_url("register/updatePass"); ?>">
        <fieldset>
            <legend class="formLegend">Ey <?= $name ?>, indícanos tu nueva password</legend>


            <div class="control-group">
                <label class="control-label" for="pass">Password</label>
                <div class="controls">
                    <input type="password" class="input-xlarge" id="pass" name="pass">
                    <?php echo form_error('pass'); ?>
                    <p class="help-block">Tu contraseña. La guardamos encriptada, no te preocupes!</p>
                </div>
            </div>


            <div class="control-group">
                <label class="control-label" for="confirmPassword">Repite Password</label>
                <div class="controls">
                    <input type="password" class="input-xlarge" id="confirmPassword" name="confirmPassword">
                    <?php echo form_error('confirmPassword'); ?>
                    <p class="help-block"></p>
                </div>
            </div>

            <div class="text-center">
                <input type="submit" class="btn btn-primary" value="Actualizar"></input>
            </div>
        </fieldset>
    </form>
</div><!--/span-->
<?php
require ('application/views/tiles/footer.php');
?>   