
<!-- jQuery -->
<script type="text/javascript" charset="utf8" src="http://ajax.aspnetcdn.com/ajax/jQuery/jquery-1.8.2.min.js"></script>

<!-- DataTables -->
<script type="text/javascript" charset="utf8" src="http://ajax.aspnetcdn.com/ajax/jquery.dataTables/1.9.4/jquery.dataTables.min.js"></script>

<script src="<?= $this->config->item('base_url') ?>/resources/jquery/js/jquery-ui-1.8.23.custom.min.js"></script>
<script src="<?= $this->config->item('base_url') ?>/resources/bootstrap/js/bootstrap.min.js"></script>
<script src="<?= $this->config->item('base_url') ?>/resources/js/jquery.blockUI.js"></script>