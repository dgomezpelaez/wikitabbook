<?php
if (!isset($onlyBody)) {
    require ('application/views/tiles/head.php');
    require('application/views/tiles/header.php');
    require('application/views/tiles/menu.php');
}
?>
<div class="fieldset fieldset-historyTab">

    <?php if(sizeof($history)<2) { ?>
    
        <h2>Revisiones</h2>
        <p>Este tab no tiene revisiones.</p>

    <?php } else { ?>
    
    <div class="page-header">
        <h3>Estas son las revisiones</h3>
    </div>
    <table class="table table-striped">
        <thead>
            <tr>
                <th style="width:100px;">Fecha Revisión</th>
                <th style="width:150px;">Mostrar diff con</th>
                <th style="width:100px;">Usuario</th>
                <th>Comentario</th>
            </tr>
        </thead>
        <tbody

            <?php
            echo '<tr>';
            echo '<td>' . $history[0]['DATE'] . '</td>';
            echo '<td>act | <a href="' . site_url('tab/diff/' . $history[0]['TAB_REV_ID'] . '/' . $history[1]['TAB_REV_ID']) . '">prev</a></td>';
            echo '<td>' . $history[0]['USERNAME'] . '</td>';
            echo '<td>' . $history[0]['COMMIT_COMMENT'] . '</td>';
            echo '</tr>';
            for ($i = 1; $i < sizeof($history) - 1; $i++) {
                echo '<tr>';
                echo '<td>' . $history[$i]['DATE'] . '</td>';
                echo '<td><a href="' . site_url('tab/diff/' . $history[0]['TAB_REV_ID'] . '/' . $history[$i]['TAB_REV_ID']) . '">act</a> | <a href="' . site_url('tab/diff/' . $history[$i]['TAB_REV_ID'] . '/' . $history[$i + 1]['TAB_REV_ID']) . '">prev</a></td>';
                echo '<td>' . $history[$i]['USERNAME'] . '</td>';
                echo '<td>' . $history[$i]['COMMIT_COMMENT'] . '</td>';
                echo '</tr>';
            }
            echo '<tr>';
            echo '<td>' . $history[$i]['DATE'] . '</td>';
            echo '<td><a href="' . site_url('tab/diff/' . $history[0]['TAB_REV_ID'] . '/' . $history[$i]['TAB_REV_ID']) . '">act</a> | prev</td>';
            echo '<td>' . $history[$i]['USERNAME'] . '</td>';
            echo '<td>' . $history[$i]['COMMIT_COMMENT'] . '</td>';
            echo '</tr>';
            ?>
    </tbody>
</table>

    <?php } ?>

</div>


<?php
if (!isset($onlyBody)) {
    $customScripts = array('tabForm');
    require ('application/views/tiles/footer.php');
}
?>   
