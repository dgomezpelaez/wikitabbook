<?php
if (!isset($onlyBody)) {
    require ('application/views/tiles/head.php');
    require('application/views/tiles/header.php');
    require('application/views/tiles/menu.php');
}
?>
<div class="fieldset fieldset-editTab">
    <form class="form-horizontal" method="post" id="tabForm" action="<?= site_url("tab/update"); ?>">
        <input type="hidden" name="tabId" value="<?= $tabId ?>">
        <input type="hidden" id="songTitle" name="songTitle" value="<?= $songTitle ?>">
        <fieldset style="width:90%;">
            <legend class="formLegend">Modifica los artistas, discos, notación o el Tab en ChordPro</legend>
           

            <div class="control-group">
                <label class="control-label" for="tabArtists">¿Quién la toca?</label>
                <div class="controls">
                    <input type="text" id="addArtistAutocomplete" placeholder="Nombre del artista"  name="artistName" class="input-append" id="artistName">
                    <span id="noArtistFoundMessage" style="display:none;"><input type="button" id="addArtistButton" class="btn btn-primary" value="Añadir"></input>
                        Ey! No lo encontramos. Dale a "Añadir" para incluirlo en WikiTabBook</span>
                </div>

            </div>
            <div id="selectedArtistsWrap" style="" class="control-group">
                <label for="tabArtists" class="control-label">Artistas Seleccionados:</label>
                <div class="controls">
                    <span style="float:left; padding-top:5px;" id="selectedArtists">
                        <?php
                        foreach ($artists as $artist) {
                            echo '<input type="hidden" value="' . $artist['ARTIST_ID'] . '" name="artistId[]" id="' . $artist['ARTIST_ID'] . '">';
                            echo '<span id="artist_' . $artist['ARTIST_ID'] . '" style="font-style:italic;">' . $artist['ARTIST_NAME'] . '<a href="javascript:removeArtist(\'' . $artist['ARTIST_ID'] . '\');">(x)</a>,</span>';
                        }
                        ?>

                </div>
            </div>


            <div class="control-group">
                <label class="control-label" for="tabAlbums">¿En qué disco?</label>
                <div class="controls">
                    <input type="text" id="addAlbumAutocomplete" placeholder="Título del disco"  name="albumName" class="input-append" id="albumName">
                    <span id="noAlbumFoundMessage" style="display:none;">
                        <input type="button" id="addAlbumButton" class="btn btn-primary" value="Añadir"></input>
                        Ey! No lo encontramos. Dale a "Añadir" para incluirlo en WikiTabBook</span>
                </div>

            </div>

            <div class="control-group" id="selectedAlbumsWrap" >
                <label class="control-label" for="tabAlbums">Discos Seleccionados:</label>
                <div class="controls">
                    <span id="selectedAlbums" style="float:left; padding-top:5px;">
                        <?php
                        foreach ($albums as $album) {
                            echo '<input type="hidden" value="' . $album['ALBUM_ID'] . '" name="albumId[]" id="' . $album['ALBUM_ID'] . '">';
                            echo '<span id="album_' . $album['ALBUM_ID'] . '" style="font-style:italic;">' . $album['ALBUM_TITLE'] . '<a href="javascript:removeAlbum(\'' . $album['ALBUM_ID'] . '\');">(x)</a>,</span>';
                        }
                        ?>
                    </span>
                </div>
            </div>	

            <div class="control-group">
                <label class="control-label" for="tabNotation">Notación</label>
                <div class="controls">
                    <select class="input-xlarge" id="tabNotation" name="tabNotation">
                        <option value="english" <?php if ($notation == 'english') echo 'selelcted'; ?>>Inglesa</option>
                        <option value="spanish" <?php if ($notation == 'spanish') echo 'selelcted'; ?>>Española</option>
                    </select>
                    <p class="help-block">Inglesa: A, B, C, D, E, F, G - Española: LA, SI, DO, RE, MI, FA, SOL</p>
                </div>
            </div>

            <div class="control-group">
                <label class="control-label" for="tab">Tab en ChordPro</label>
                <div class="controls">
                    <textarea rows="20" style="width:80%;" id="tab" name="tab"><?= $tab ?></textarea>
                </div>
            </div>
              <div class="control-group">
                <label class="control-label" for="commitComment">Comentario del cambio</label>
                <div class="controls">
                    <input type="text" class="input-xxlarge typeahead" id="commitComment" name="commitComment">
                    <?php echo form_error('commitComment'); ?>
                </div>
            </div>
            <div class="text-center">
                <input type="button" onclick="javascript:loadPrev();return false;" class="btn btn-primary" value="Previsualizar y/o Guardar"></input>
            </div>
        </fieldset>
    </form>
</div><!--/span-->
<div id="prevDiv" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="Prev" aria-hidden="true">
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">Previsualización</button>
        <h3>Previsualización</h3>
    </div>
    <div class="modal-body">
        <div id="prevTab"></div>
    </div>
    <div class="modal-footer">
        <input type="button" onclick="$('#prevDiv').modal('hide')" class="btn btn-primary" value="Volver al editor"></input>
        <input type="button" onclick="javascript:saveTab();return false;" class="btn btn-primary" value="Guardar"></input>
    </div>
</div>

</div>


<?php
if (isset($onlyBody)) {
    $customScripts = array('tabForm');

} else {
    $customScripts = array('tabForm');
    require ('application/views/tiles/footer.php');
}
?>   

