<?php
require('application/views/tiles/head.php');
require('application/views/tiles/header.php');
require('application/views/tiles/menu.php');
?> 

<div class="span11 mainContent">
    <h4 style="display:inline;position: relative;top: 33px;z-index: 200;left: 20px;">Resultados de buscar: <?= $searchValue; ?></h4>
    <div id="tabs">
        <ul>

            <li style="float:right;"><a href="<?= site_url("home/search?searchIn=albums&search=" . $searchValue); ?>">Discos</a></li>
            <li style="float:right;"><a href="<?= site_url("home/search?searchIn=artists&search=" . $searchValue); ?>">Artistas</a></li>
            <li style="float:right;"><a href="#tabs-songs">Canciones</a></li>

        </ul>
        <div id="tabs-songs">
            <?php
            require ('application/views/searchResultsDinamic.php');
            ?>
        </div>
    </div>

</div>
<?php
$customScripts = array('search');
require ('application/views/tiles/footer.php');
?>   